#!/bin/bash

test_unexported_function() {
    myprog() { perl -e 'exit (shift > 12345678)' "$@"; }
    # myprog is a function, so source find-first-fail first
    . "$(command -v find-first-fail)"
    echo Find 12345678 in unexported function
    find-first-fail myprog
}

test_exported_function() {
    myprog() { perl -e 'exit (shift > 12345678)' "$@"; }
    # myprog is an exported function
    export -f myprog
    echo Find 12345678
    find-first-fail myprog
}

test_startvalue() {
    # exit value changes at 100 and 12345678
    myprog() { perl -e '$a=shift;if($a <= 123) { exit 0; }
    	       	        else { exit ($a <= 12345678) }' "$@"; }
    export -f myprog
    echo Find 123
    find-first-fail myprog
    echo Find 12345678
    find-first-fail -s 200 myprog
}

test_s_v_12() {
    # Multiple options
    myprog() { perl -e 'exit (shift > 12)' "$@"; }
    export -f myprog
    echo Find 12 with progress
    find-first-fail -v -s 10 myprog
    echo Find 12 with progress quiet
    find-first-fail -v -q -s 10 myprog
}

test_file() {
    tmp=$(mktemp)
    (echo Header
     seq 100
     echo 10
     echo 12
     echo 15) > "$tmp"
    10_to_15() { grep ^10$ "$1" && grep ^15$ "$1"; }
    export -f 10_to_15
    echo 10..15
    find-first-fail -s1 -qf "$tmp" 10_to_15
    echo not 10..15
    find-first-fail -s1 -qf "$tmp" not 10_to_15
    rm "$tmp"
}

test_header() {
    tmp=$(mktemp)
    (echo Header
     seq 10
     echo 1000
     seq 10) > "$tmp"
    myparser() { perl -ne 'if($_ > 100) { exit 1 }' "$@"; }
    export -f myparser
    echo Should give:
    echo Header
    echo 1000
    find-first-fail -s1 -f "$tmp" myparser
}

test_file_start_middle_end() {
    tmp=$(mktemp)
    testfile() {
	(echo MyHeader
	 echo "$1"
	 echo 2
	 echo "$2"
	 echo 4
	 echo "$3") > "$tmp"
    }
    myparser() { perl -ne 'if($_ > 10) { exit 1 }' "$@"; }
    export -f myparser
    echo 'Find 1001'
    testfile 1001 3 5
    find-first-fail -s1 -f "$tmp" myparser
    echo 'Find 1003'
    testfile 1 1003 5
    find-first-fail -s1 -f "$tmp" myparser
    echo 'Find 1005'
    testfile 1 3 1005
    find-first-fail -s1 -f "$tmp" myparser
    echo 'Find 1001 or 1005'
    testfile 1001 3 1005
    find-first-fail -s1 -f "$tmp" myparser
    echo 'Find 1001 or 1003 or 1005'
    testfile 1001 1003 1005
    find-first-fail -s1 -f "$tmp" myparser
}

test_end() {
    echo 'Find 12'
    myprog() { perl -e 'exit (shift > 12)' "$@"; }
    export -f myprog
    find-first-fail -v -e 13 myprog
    find-first-fail -v -s 11 -e 15 myprog
}

test_dashdash() {
    echo 'Test -- will allow a function named -q'
    -q() { perl -e 'exit (shift > 12)' "$@"; }
    export -f -- -q
    find-first-fail -q -- -q
}

export -f $(compgen -A function | grep test_)
compgen -A function | grep test_ | LC_ALL=C sort |
    parallel --timeout 1000% --tag -k --joblog /tmp/jl-"$(basename "$0")" '{} 2>&1'
